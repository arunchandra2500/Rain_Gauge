/*
 * raingauge_station.h
 *
 *  Created on: 16-May-2022
 *      Author: arun
 */

#ifndef WEATHER_STATION_RAINGAUGE_STATION_H_
#define WEATHER_STATION_RAINGAUGE_STATION_H_


#define BATT_POWER    	1

typedef struct {


	uint16_t rainfall;

    uint16_t batteryLevel;

    uint16_t totalrainfall;

    uint16_t downlinkRainfall;


}rainfallData_t;

uint16_t readBatteryLevel(void);

void rainGaugeInterruptEnable();
void rainGaugeTips();
uint16_t getRainfall();
uint16_t getTotalRainfall();

void enable(uint8_t);
void disable(uint8_t);
void raingaugeStationInit();
void readRaingaugeStationParameters(rainfallData_t *sensor_data);
void raingaugeStationGPIO_Init();
void readRainAccumulation(rainfallData_t *sensor_data);


#endif /* WEATHER_STATION_RAINGAUGE_STATION_H_ */
