/*
 * config.h
 *
 *  Created on: 17-May-2022
 *      Author: arun
 */

#ifndef RAINGAUGE_STATION_CONFIG_H_
#define RAINGAUGE_STATION_CONFIG_H_

/*
 * config.h
 *
 *  Created on: 30-Jul-2021
 *      Author: Ajmi
 *      @about A single config file to configure the lorawan parameters without editing the main files.
 */


#ifndef OVER_THE_AIR_ACTIVATION
#define OVER_THE_AIR_ACTIVATION                            0
#endif



/************************************Extra features that can be enabled here**********************************/
//Enable and Disable Modules

//#define ENABLE_SI1145_UVINDEX 			    /* uncomment to enable comment to disable */
//#define ENABLE_BME280					/* uncomment to enable comment to disable */
//#define ENABLE_SHT20

/************************************Extra features that can be enabled here**********************************/


/************************************Device Key Configuration***************************************/

#define DEVICE_EUI 						 { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 }

#define JOIN_EUI                                                {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

#define OTAA_APP_KEY						{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00}


#if !OVER_THE_AIR_ACTIVATION

#define DEVICE_ADDRESS 						( uint32_t )0x00000000

#define NETWORK_SESSION_KEY					{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00}

#define APP_SESSION_KEY						{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00}

#endif

/************************************Device Key Configuration***************************************/



/************************************Device Operation Configuration***************************************/

#define SEND_INTERVAL						30000              /*the application data transmission duty cycle*/

#define ADR_STATE 						LORAWAN_ADR_OFF	   /*LoRaWAN Adaptive Data Rate * @note Please note that when ADR is enabled the end-device should be static  check commissioning.h*/

#define DATA_RATE						DR_5

#define APP_PORT						2

#define CLASS_OPERATION						CLASS_A

#define UPLINK_MSG_STATE					LORAWAN_UNCONFIRMED_MSG

/************************************Device Operation Configuration***************************************/




#endif /* RAINGAUGE_STATION_CONFIG_H_ */
