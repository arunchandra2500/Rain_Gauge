/*
 * raingauge_station.c
 *
 *  Created on: 16-May-2022
 *      Author: arun
 */

#include <string.h>
#include <stdlib.h>
#include "hw.h"

#include "raingauge_station.h"
#include "util_console.h"
#include "lora.h"



#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/


uint16_t rainGaugetipCount = 0; /*   rainCount increment for every interrupt rain fall */
LoraFlagStatus dailyCountFlag = LORA_RESET;
uint16_t TotalAccumulatedRainfall=0;

/*  readBatteryLevel   */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun-- ICFOSS
 *
 * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
 * @param  none
 * @retval battery voltage level
 *
 **/
uint16_t readBatteryLevel(void) {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* enable battery voltage reading */
	enable(BATT_POWER);

	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(ADC_CHANNEL_4);

	/* disable battery voltage reading */
	disable(BATT_POWER);

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}

/*  rainGaugeInterruptEnable  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note this pin PA0 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeInterruptEnable() {

	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;

	HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, rainGaugeTips);
}

/*  rainGaugeTips  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief callback function interrupt on PA9 pin
 * @note this pin PA0 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeTips() {
	rainGaugetipCount++;
	PRINTF("rainGaugetipCount=%d", rainGaugetipCount);

}

/*  getAccumulatedRainfall  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief get total accumulated rainfall at transmission interval
 * @note after transmission of rainfall the data is reset and starts new count
 * @param none
 * @retval total rainfall at specified interval
 *
 **/
uint16_t getAccumulatedRainfall() {
	uint16_t rainfall = 0;
	rainfall = ((float) rainGaugetipCount * 0.2) * 100;
	PRINTF("rainfall=%d", rainfall);
	TotalAccumulatedRainfall+=rainfall;
	rainGaugetipCount = 0;
	return (uint16_t) rainfall;

}
uint16_t getTotalRainfall(){
	uint16_t TotalRainfall=0;
	if(dailyCountFlag == LORA_RESET){
		TotalRainfall=TotalAccumulatedRainfall;
		PRINTF("TotalRainfall=%d", TotalRainfall);
	}
	else{
		//TotalRainfall=0;
		TotalAccumulatedRainfall=0;
		PRINTF("TotalRainfall=%d", TotalRainfall);
		dailyCountFlag = LORA_RESET;
	}
   return (uint16_t) TotalRainfall;
}

uint16_t getdownlinkRainfall(){
	uint16_t dailRainfall=0;
	dailRainfall=getTotalRainfall();
	dailyCountFlag = LORA_SET;
	return (uint16_t)dailRainfall;


}
void raingaugeStationGPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HW_GPIO_Init(GPIOB, GPIO_PIN_2, &initStruct);
//	HW_GPIO_Init(GPIOB, GPIO_PIN_6, &initStruct);
}

/*  enable  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief manual control of gpio
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {

		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET); //for battery

}

/*  disable  */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief manual control of gpio
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {

		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET); //for battery

}

/*  raingaugeStationInit */
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief initialize  raingauge station system
 * @param none
 * @retval none
 *
 **/

void raingaugeStationInit() {

	rainGaugeInterruptEnable();
	raingaugeStationGPIO_Init();

}

/*  readWeatherStationParameters*/
/**
 * Created on: May 16, 2022
 * Last Edited: May 16, 2022
 * Author: Arun
 *
 * @brief initialize  weather station system
 * @param none
 * @retval none
 *
 **/
void readRaingaugeStationParameters(rainfallData_t *sensor_data) {

	sensor_data->batteryLevel = readBatteryLevel();

	sensor_data->rainfall = getAccumulatedRainfall();

	sensor_data->totalrainfall = getTotalRainfall();

}

/*  readRainAccumulation*/
/**
 * Created on: Jun 1, 2022
 * Last Edited: Jun 1, 2022
 * Author: Arun
 *
 * @brief read and reset accumulated Rainfall data
 * @param none
 * @retval none
 *
 **/
void readRainAccumulation(rainfallData_t *sensor_data)
{
	sensor_data->downlinkRainfall = getdownlinkRainfall();

}



